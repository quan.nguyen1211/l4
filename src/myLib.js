/**
 * This arrow function returns the sum of two parameters
 * Without curly brackets it doesnt require return key
 * @param {*} param1 this is the first parameter    
 * @param {*} param2 this is the second parameter
 * @returns {number}
 */

const add = (param1, param2) =>  {
    const result = param1 + param2;
    return result
}

const multiply = (a,b) => a*b;

const divide = (a,b) => {
    c=1;
    try {
        c = a/b;
    }
    catch (err) {
        return "can not divide 0"
    }
    return c;
}

const subtract = (a,b) => a-b;

module.exports = {
    add,
    subtract,
    multiply,
    divide,
};