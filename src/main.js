// src/main.js
const express = require("express");
const req = require("express/lib/request");
const res = require("express/lib/response");
const mylib = require("./myLib.js");
const app = express();
const port = 65535;

app.get('/add', (req,res)=>{
    const a = req.query.a;
    const b = req.query.b;
    const  sum = mylib.add(a,b);
    res.send(sum.toString());
});

app.get('/multiply', (req,res)=>{
    const a = req.query.a;
    const b = req.query.b;
    const  sum = mylib.multiply(a,b);
    res.send(sum.toString());
});

app.get('/divide', (req,res)=>{
    const a = req.query.a;
    const b = req.query.b;
    const  sum = mylib.divide(a,b);
    res.send(sum.toString());
});

app.get('/subtract', (req,res)=>{
    const a = req.query.a;
    const b = req.query.b;
    const  sum = mylib.subtract(a,b);
    res.send(sum.toString());
});

app.listen(port,()=>{
    console.log(`Server: http://localhost:${port}`)
});