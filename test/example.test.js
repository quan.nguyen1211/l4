// example.test.js
const { describe, before } = require('mocha');
const {expect} = require('chai');
const mylib = require('../src/myLib.js');

before(() => {
    console.log(mylib.random);
})

describe('Unit testing mylib.js', () => {
    it('Should return 2 when using sum function with a=1, b=1', () => {
        const res = mylib.add(1, 1);
        expect(res).to.equal(2); // result expected to equal 2
    });

    it('Should return 5 when using sum function with a=2, b=3', () => {
        const res = mylib.add(2, 3);
        expect(res).to.equal(5); // result expected to equal 2
    });

    it('Should return -1 when using sum function with a=-2, b=1', () => {
        const res = mylib.add(-2, 1);
        expect(res).to.equal(-1); // result expected to equal 2
    });

    it('Should return 0 when using sum function with a=-5, b=-5', () => {
        const res = mylib.add(-5, 5);
        expect(res).to.equal(0); // result expected to equal 2
    });

    it('Should return "HelloWorld" when using sum function with a=Hello, b=World', () => {
        const res = mylib.add("Hello", "World");
        expect(res).to.equal("HelloWorld"); // result expected to equal 2
    });

    it('Should return 2 when using multiply function with a=2, b=3', () => {
        const res = mylib.multiply(2, 3);
        expect(res).to.equal(6); // result expected to equal 6
    });

    it('Should return 2 when using divide function with a=4, b=2', () => {
        const res = mylib.divide(4, 2);
        expect(res).to.equal(2); // result expected to equal 2
    });

    it('Should return -1 when using multiply function with a=2, b=3', () => {
        const res = mylib.subtract(2, 3);
        expect(res).to.equal(-1); // result expected to equal -1
    });
});

after(() => {
    console.log(mylib.goodbye);
})
